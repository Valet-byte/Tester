package com.example.mongo_repo_micro;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class MongoRepoMicroApplication {

    public static void main(String[] args) {
        SpringApplication.run(MongoRepoMicroApplication.class, args);
    }

    @Autowired
    public void printMsj(@Value("${msj}") String msj){
        System.out.println(msj);
    }

}

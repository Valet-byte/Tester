package com.example.mongo_repo_micro.repo;

import com.example.mongo_repo_micro.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface UserRepo extends MongoRepository<User, String> {
    List<User> findByName(String name);
}

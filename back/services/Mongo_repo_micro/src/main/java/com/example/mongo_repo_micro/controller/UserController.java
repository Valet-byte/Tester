package com.example.mongo_repo_micro.controller;

import com.example.mongo_repo_micro.model.User;
import com.example.mongo_repo_micro.repo.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class UserController {

    @Autowired
    private UserRepo repo;

    @GetMapping("/findById")
    public User findById(@RequestParam("ID") String id){
        return repo.findById(id).orElse(null);
    }

    @GetMapping("/findByName")
    public User findByName(@RequestParam("name") String name){
        System.out.println(name);
        try{
            User user = repo.findByName(name).get(0);
            return user;
        }catch (Exception ignore){
            return null;
        }
    }

    @PostMapping("/save")
    public void save(@RequestBody User user){

        repo.save(user);
    }

}

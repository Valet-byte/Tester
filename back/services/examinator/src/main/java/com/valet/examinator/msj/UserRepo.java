package com.valet.examinator.msj;

import com.valet.examinator.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.Optional;

@Component
public class UserRepo {

    private final RestTemplate template;

    public UserRepo(RestTemplateBuilder builder) {
        template = builder.build();
    }

    @Value("${name_repo}")
    private String nameRepo;

    public Optional<User> findById(String id){
        Optional<User> user = Optional.ofNullable(template.getForObject("http://localhost:8787/findById?ID="+id, User.class));

        return user;
    }

    public User findByName(String name){
        return template.getForObject("http://localhost:8787/findByName?name="+name, User.class);
    }

    public void save(User user) {
        template.postForObject("http://localhost:8787/save", user, Void.TYPE);
    }
}
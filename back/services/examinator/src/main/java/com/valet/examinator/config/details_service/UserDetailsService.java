package com.valet.examinator.config.details_service;

import com.valet.examinator.model.User;
import com.valet.examinator.msj.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
public class UserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {

    @Autowired
    UserRepo repo;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = repo.findByName(username);
        if (user == null) throw new UsernameNotFoundException("not found");
        return getUserDetails(user);
    }

    private static UserDetails getUserDetails(User user){
        return new org.springframework.security.core.userdetails.User(user.getName(),
                user.getPass(), getDefaultGrantedAuthority());
    }

    private static Collection<? extends GrantedAuthority> getDefaultGrantedAuthority(){
        List<GrantedAuthority> authorities = new ArrayList<>(1);
        authorities.add(new SimpleGrantedAuthority("USER"));
        return authorities;
    }
}

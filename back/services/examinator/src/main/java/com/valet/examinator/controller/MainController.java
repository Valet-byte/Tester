package com.valet.examinator.controller;

import com.valet.examinator.model.Exam;
import com.valet.examinator.model.Question;
import com.valet.examinator.model.Section;
import com.valet.examinator.model.User;
import com.valet.examinator.msj.UserRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.security.Principal;
import java.util.*;

@RestController
@CrossOrigin
@RequiredArgsConstructor
public class MainController {

    private final RestTemplate template;
    private final UserRepo repo;
    private final PasswordEncoder encoder;

    @Value("${msj}")

    private String msj;

    @PostConstruct
    private void postConstruct(){
        System.out.println(msj);
    }

    @RequestMapping("/exam")
    public Exam getExam(@RequestBody Map<String, Integer> spec){
        List<Section> sections = new ArrayList<>(spec.size());

        spec.entrySet().forEach((entry -> {
            String url = getURL(entry);
            List<Question> questions = Arrays.asList(Objects.requireNonNull(template.getForObject(url, Question[].class)));
            sections.add(new Section(getNameSubject(entry.getKey()), questions));
        }));

        System.out.println(spec);

        return Exam.builder().title("EXAM").sections(sections).build();
    }

    @PostMapping("/registration")
    public boolean registration(@RequestBody User user){
        if (getExistUser(user.getName())){
            user.setPass(encoder.encode(user.getPass()));
            repo.save(user);
            return true;
        } else {
            return false;
        }
    }

    @RequestMapping("/auth")
    public User auth(Principal principal){
        return User.builder().name(principal.getName()).build();
    }

    @PostMapping("/addQuestions")
    public boolean addQuestions(@RequestBody Exam exam){
        System.out.println(exam);
        if (exam.getTitle().equals("load")){
            exam.getSections()
                    .forEach((sec) -> {

                        if (!sec.getQuestions().get(0).equals(new Question("", ""))){
                            template.postForObject("http://" + sec.getTitle() + "/questions",
                                    sec.getQuestions().get(0), Void.TYPE);
                        }
                    });
        }
        return true;
    }

    private boolean getExistUser (String name){
        User user = repo.findByName(name);
        return user == null;
    }
    private String getURL (Map.Entry<String, Integer> entry){
        return "http://"+entry.getKey()+"/api/questions?amount="+entry.getValue();
    }
    private String getNameSubject(String appName){
        return switch (appName) {
            case "MATH" -> "Математика";
            case "HISTORY" -> "История";
            case "INFORMATION" -> "Информатика";
            default -> appName;
        };
    }
}
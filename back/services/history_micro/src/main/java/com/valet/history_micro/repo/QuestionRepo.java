package com.valet.history_micro.repo;

import com.valet.history_micro.model.Question;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface QuestionRepo extends MongoRepository<Question, String> {
}

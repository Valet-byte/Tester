package com.valet.history_micro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HistoryMicroApplication {

    public static void main(String[] args) {
        SpringApplication.run(HistoryMicroApplication.class, args);
    }

}

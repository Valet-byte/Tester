package com.valet.micro_math.repo;

import com.valet.micro_math.model.Question;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface MongoQuestionRepo extends MongoRepository<Question, String> {
}

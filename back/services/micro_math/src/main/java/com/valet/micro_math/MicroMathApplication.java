package com.valet.micro_math;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MicroMathApplication {

    public static void main(String[] args) {
        SpringApplication.run(MicroMathApplication.class, args);
    }

}

package com.valet.micro_math.controller;

import com.valet.micro_math.model.Question;
import com.valet.micro_math.repo.MongoQuestionRepo;
import com.valet.micro_math.service.MathService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
public class MathController {

    @Autowired
    private MongoQuestionRepo repo;
    @Value("${msj}")
    private String msj;

    @PostConstruct
    private void postConstruct(){
        System.out.println(msj);
    }

    @GetMapping("/questions")
    public List<Question> getRandomQuestions(@RequestParam int amount){
        List<Question> questions = repo.findAll();
        Collections.shuffle(questions);
        return questions.stream().limit(amount).collect(Collectors.toList());
    }

}

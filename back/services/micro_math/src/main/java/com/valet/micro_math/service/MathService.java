package com.valet.micro_math.service;

import com.valet.micro_math.model.Question;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service
public class MathService {

    private final Random random = new Random();
    private final int max = 10;

    public Question getRandom(){
        int a = random.nextInt(max);
        int b = random.nextInt(max);
        return Question.builder().question(a+" + "+b+" = ?").answer(String.valueOf(a + b)).build();
    }

}

package com.valet.information_micro.controller;

import com.valet.information_micro.model.Question;
import com.valet.information_micro.repo.QuestionRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
public class Controller {

    @Autowired
    QuestionRepo repo;

    @Value("${msj}")
    private String msj;

    @PostConstruct
    private void postConstruct(){
        System.out.println(msj);
    }

    @GetMapping("/questions")
    public List<Question> getQuestions(@RequestParam int amount){
        List<Question> questions = repo.findAll();
        Collections.shuffle(questions);
        return questions.stream().limit(amount).collect(Collectors.toList());
    }


}

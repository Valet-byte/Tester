package com.valet.information_micro.repo;


import com.valet.information_micro.model.Question;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface QuestionRepo extends MongoRepository<Question, String> {
}

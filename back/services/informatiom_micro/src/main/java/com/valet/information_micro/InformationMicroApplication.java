package com.valet.information_micro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InformationMicroApplication {

    public static void main(String[] args) {
        SpringApplication.run(InformationMicroApplication.class, args);
    }

}

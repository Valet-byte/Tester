import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {MyHttpclient} from "../http.client/app.httpclient.myhttpclient";

@Component({
  selector: 'app-out-component',
  templateUrl: './out-component.html',
  styleUrls: ['./out-component.css']
})
export class OutComponent implements OnInit {

  constructor(private router: Router, private client: MyHttpclient) { }

  ngOnInit(): void {
    this.client.logout()
    this.router.navigateByUrl('/').then(() => {})
  }

}

import { Component, OnInit } from '@angular/core';
import {MyHttpclient} from "../http.client/app.httpclient.myhttpclient";
import {Router} from "@angular/router";

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  name : string = ''
  password : string = ''
  visibleError : boolean

  constructor(private client: MyHttpclient,
              private router: Router) {
    this.visibleError = false
  }

  ngOnInit(): void {
  }

   onClick(name: string, pass:string){
    console.log(name)
    console.log(pass)
     console.log(this.password)
     console.log(this.name)
    this.client.registration(name, pass).subscribe(data => {
      if (data){
        this.visibleError = false
        this.router.navigateByUrl('/auth').then()
      }
      else {
        this.visibleError = true
        pass = ''
        name = ''
      }
    })
  }

}

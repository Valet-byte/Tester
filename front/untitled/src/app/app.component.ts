import {Component, OnInit} from '@angular/core';
import {MyHttpclient} from "./http.client/app.httpclient.myhttpclient";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{

  constructor() {
  }

  ngOnInit(): void {}

  getUserName(){
    return MyHttpclient.getUserName()
  }

  getIsLogin() {
    return MyHttpclient.isLogin()
  }
}

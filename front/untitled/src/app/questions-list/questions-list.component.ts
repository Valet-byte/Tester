import { Component, OnInit } from '@angular/core';
import {Exam} from "../model/Exam";
import {MyHttpclient} from "../http.client/app.httpclient.myhttpclient";

@Component({
  selector: 'app-questions-list',
  templateUrl: './questions-list.component.html',
  styleUrls: ['./questions-list.component.css']
})
export class QuestionsListComponent implements OnInit {

  exam : Exam

  mathQ : number
  historyQ : number
  infQ: number;

  constructor(private client : MyHttpclient) {
    this.mathQ = 0
    this.historyQ = 0
    this.infQ = 0
    this.exam = new Exam
  }

  ngOnInit(): void {
  }

  downloadQuestions(): void{

    this.client.downloadQuestions({
      MATH: this.mathQ,
      HISTORY: this.historyQ,
      INFORMATION: this.infQ
    }).subscribe(data =>{
        this.exam = data
        console.log(data)
      }
    )
  }

  isLogin(){
    return MyHttpclient.isLogin();
  }

}

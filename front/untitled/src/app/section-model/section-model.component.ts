import {Component, Input, OnInit} from '@angular/core';
import {Section} from "../model/Section";

@Component({
  selector: 'app-section-model',
  templateUrl: './section-model.component.html',
  styleUrls: ['./section-model.component.css']
})
export class SectionModelComponent implements OnInit {

  @Input() sec : Section = new Section()

  constructor() {
  }

  ngOnInit(): void {
  }
}

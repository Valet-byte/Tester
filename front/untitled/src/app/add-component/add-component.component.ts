import { Component, OnInit } from '@angular/core';
import {MyHttpclient} from "../http.client/app.httpclient.myhttpclient";
import {Exam} from "../model/Exam";

@Component({
  selector: 'app-add-component',
  templateUrl: './add-component.component.html',
  styleUrls: ['./add-component.component.css']
})
export class AddComponentComponent implements OnInit {

  constructor(private client: MyHttpclient) { }

  ngOnInit(): void {
  }

  onClick(mathQ: string, mathA: string, historyQ: string, historyA: string, infQ: string, infA: string) {

    let ex = {
      title: 'load',
      sections: [
        {
          title: 'MATH',
          questions: [
            {
              question: mathQ,
              answer: mathA
            }
          ]
        },
        {
          title: 'HISTORY',
          questions: [
            {
              question: historyQ,
              answer: historyA
            }
          ]
        },
        {
          title: 'INFORMATION',
          questions: [
            {
              question: infQ,
              answer: infA
            }
          ]
        }
      ]
    }
    this.client.loadQuestions(ex)
  }
}

import {Injectable} from "@angular/core";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {Exam} from "../model/Exam";
import {User} from "../model/User";
import {CookieService} from "ngx-cookie-service";
import * as CryptoJS from 'crypto-js';

@Injectable({
  providedIn: 'root'
})
export class MyHttpclient {
  private static user : User = new User()
  private static key = CryptoJS.enc.Utf8.parse('neag.aklgnal.sz,x');
  private static iv = CryptoJS.enc.Utf8.parse('vlnsakgnjaiuovdfujk');

  constructor(private client : HttpClient, private cookie: CookieService ) {
    let username = cookie.get('username')
    let password = cookie.get('password')

    if (username == '' || password == ''){
      MyHttpclient.user.name = 'Авторизация'
    } else {
      MyHttpclient.user.name = username
      MyHttpclient.user.pass = this.decrypt(password)
    }
  }

  public static getUserName() : string {
    return MyHttpclient.user.name;
  }
  public static isLogin(){
    return this.user.pass != '';
  }
  downloadQuestions(body: any): Observable<Exam> {
    const headers = new HttpHeaders({ authorization: this.createBasicAuthToken(MyHttpclient.user.name,
        MyHttpclient.user.pass)});
    return this.client.post<Exam>("http://localhost:8083/exam", body, {headers})
  }
  createBasicAuthToken(username: String, password: String) {
    return 'Basic ' + btoa(username + ":"+password )
  }
  registration(name: string, password: string): Observable<boolean> {

    console.log(name)
    console.log(password)
    let user = new User();
    user.pass = password
    user.name = name

    console.log(user)
    return this.client.post<boolean>("http://localhost:8083/registration", user)

  }
  auth(name: string, password: string, doJob: any) {
    this.cookie.set('username', name)
    this.cookie.set('password', this.encrypt(password))
    const headers = new HttpHeaders({ authorization: this.createBasicAuthToken(name, password)});
    this.client.post<User>("http://localhost:8083/auth", {}, {headers}).subscribe(data => {
      MyHttpclient.user.name = data.name
      MyHttpclient.user.pass = password
      doJob.job()
    })
  }

  decrypt(pass: string): string{
    return CryptoJS.DES.decrypt(pass, MyHttpclient.key, {
      keySize: 128 / 8,
      iv: MyHttpclient.iv,
      mode: CryptoJS.mode.CBC,
      padding: CryptoJS.pad.Pkcs7
    }).toString(CryptoJS.enc.Utf8);
  }

  encrypt(pass: string): string{
    return CryptoJS.DES.encrypt(pass, MyHttpclient.key, {
      keySize: 128 / 8,
      iv: MyHttpclient.iv,
      mode: CryptoJS.mode.CBC,
      padding: CryptoJS.pad.Pkcs7
    }).toString()
  }

  logout() {
    this.cookie.set('username', '')
    this.cookie.set('password', '')
    MyHttpclient.user.name = 'Авторизация'
    MyHttpclient.user.pass = ''
  }

  loadQuestions(ex: any){
    const headers = new HttpHeaders({ authorization: this.createBasicAuthToken(MyHttpclient.user.name,
        MyHttpclient.user.pass)});

    console.log(ex)

    this.client.post<boolean>("http://localhost:8083/addQuestions", ex, {headers})
      .subscribe(data => {
        console.log(data)
      })
  }
}

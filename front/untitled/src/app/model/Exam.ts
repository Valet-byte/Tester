import {Section} from "./Section";

export class Exam{
  title?: string
  sections?: Section[]
}

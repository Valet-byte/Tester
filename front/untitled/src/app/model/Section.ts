import {Question} from "./Question";

export class Section{
  title: string = ""
  questions?: Question[]
}

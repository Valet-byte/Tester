import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { HttpClientModule} from "@angular/common/http";
import {FormsModule} from "@angular/forms";
import { SectionModelComponent } from './section-model/section-model.component';
import {AppRoutingModule} from "./app.routing";
import {RouterModule} from "@angular/router";
import { QuestionsListComponent } from './questions-list/questions-list.component';
import {AuthenticationComponent} from "./authentication/authentication.component";
import {RegistrationComponent} from "./registration/registration.component";
import {MyHttpclient} from "./http.client/app.httpclient.myhttpclient";
import {CookieService} from "ngx-cookie-service";
import {OutComponent} from "./out-component/out-component";
import { AddComponentComponent } from './add-component/add-component.component';

@NgModule({
  declarations: [
    AppComponent,
    SectionModelComponent,
    AuthenticationComponent,
    RegistrationComponent,
    QuestionsListComponent,
    OutComponent,
    AddComponentComponent
  ],
    imports: [
      BrowserModule,
      HttpClientModule,
      AppRoutingModule,
      RouterModule,
      FormsModule
    ],
  providers: [MyHttpclient, CookieService],
  bootstrap: [AppComponent]
})
export class AppModule { }

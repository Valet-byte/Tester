import { Component, OnInit } from '@angular/core';
import {MyHttpclient} from "../http.client/app.httpclient.myhttpclient";
import {Router} from "@angular/router";

@Component({
  selector: 'app-authentication',
  templateUrl: './authentication.component.html',
  styleUrls: ['./authentication.component.css']
})
export class AuthenticationComponent implements OnInit {

  constructor(private client: MyHttpclient,
              private router: Router) {
  }

  ngOnInit(): void {
  }

  onClick(name: string, password: string) {
    this.client.auth(name, password, new Worker(this.router, ""))

  }
}

export class Worker{

  constructor(private router: Router, private url: string) {
  }
  job(){
    this.router.navigateByUrl("/" + this.url).then(r  =>{})
  }
}

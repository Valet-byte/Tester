import {RouterModule, Routes} from "@angular/router";
import {NgModule} from "@angular/core"
import {QuestionsListComponent} from "./questions-list/questions-list.component";
import {AuthenticationComponent} from "./authentication/authentication.component";
import {RegistrationComponent} from "./registration/registration.component";
import {OutComponent} from "./out-component/out-component";

const routes: Routes = [
  {path:'auth', component: AuthenticationComponent},
  {path:'registration', component: RegistrationComponent},
  {path:'out', component: OutComponent},
  {path:'', component: QuestionsListComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
